# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141013085630) do

  create_table "base_categories", force: true do |t|
    t.integer  "parent_id"
    t.string   "code"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "base_categories", ["code"], name: "index_base_categories_on_code", using: :btree

  create_table "base_classifieds", force: true do |t|
    t.string   "code"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "base_classifieds", ["code"], name: "index_base_classifieds_on_code", using: :btree

  create_table "base_colors", force: true do |t|
    t.string   "code"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "base_colors", ["code"], name: "index_base_colors_on_code", using: :btree

  create_table "base_customers", force: true do |t|
    t.string   "original"
    t.string   "area"
    t.string   "parent"
    t.string   "level"
    t.string   "storename"
    t.string   "login"
    t.string   "code"
    t.string   "password"
    t.integer  "amount"
    t.integer  "quantity"
    t.boolean  "if_enable"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "base_customers", ["area"], name: "index_base_customers_on_area", using: :btree
  add_index "base_customers", ["login"], name: "index_base_customers_on_login", unique: true, using: :btree
  add_index "base_customers", ["original"], name: "index_base_customers_on_original", using: :btree
  add_index "base_customers", ["storename"], name: "index_base_customers_on_storename", using: :btree

  create_table "base_periods", force: true do |t|
    t.string   "code"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "base_periods", ["code"], name: "index_base_periods_on_code", using: :btree

  create_table "base_series", force: true do |t|
    t.string   "code"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "base_series", ["code"], name: "index_base_series_on_code", using: :btree

  create_table "base_sizes", force: true do |t|
    t.string   "class"
    t.integer  "index"
    t.string   "name"
    t.string   "value"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "base_sizes", ["class"], name: "index_base_sizes_on_class", using: :btree
  add_index "base_sizes", ["name"], name: "index_base_sizes_on_name", using: :btree

  create_table "product_colors", force: true do |t|
    t.integer  "product_id"
    t.integer  "base_color_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "product_colors", ["base_color_id"], name: "index_product_colors_on_base_color_id", using: :btree
  add_index "product_colors", ["product_id"], name: "index_product_colors_on_product_id", using: :btree

  create_table "product_images", force: true do |t|
    t.integer  "product_id"
    t.string   "url"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "product_images", ["product_id"], name: "index_product_images_on_product_id", using: :btree

  create_table "product_sizes", force: true do |t|
    t.integer  "product_id"
    t.integer  "base_size_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "product_sizes", ["base_size_id"], name: "index_product_sizes_on_base_size_id", using: :btree
  add_index "product_sizes", ["product_id"], name: "index_product_sizes_on_product_id", using: :btree

  create_table "product_stars", force: true do |t|
    t.integer  "product_id"
    t.integer  "product_color_id"
    t.integer  "base_customer_id"
    t.integer  "star"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "product_stars", ["base_customer_id"], name: "index_product_stars_on_base_customer_id", using: :btree
  add_index "product_stars", ["product_color_id"], name: "index_product_stars_on_product_color_id", using: :btree
  add_index "product_stars", ["product_id"], name: "index_product_stars_on_product_id", using: :btree

  create_table "products", force: true do |t|
    t.string   "number"
    t.string   "name"
    t.string   "size_class"
    t.decimal  "price",              precision: 7, scale: 2
    t.integer  "series_id"
    t.integer  "period_id"
    t.integer  "classified_id"
    t.integer  "category_id"
    t.integer  "sub_category_id"
    t.string   "recommended_level"
    t.text     "description_fabric"
    t.text     "description_style"
    t.boolean  "if_ordering"
    t.boolean  "if_replenishing"
    t.boolean  "if_onsale"
    t.boolean  "if_preselection"
    t.string   "sex"
    t.string   "catena"
    t.date     "topdate"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "products", ["if_onsale"], name: "index_products_on_if_onsale", using: :btree
  add_index "products", ["if_ordering"], name: "index_products_on_if_ordering", using: :btree
  add_index "products", ["if_preselection"], name: "index_products_on_if_preselection", using: :btree
  add_index "products", ["if_replenishing"], name: "index_products_on_if_replenishing", using: :btree
  add_index "products", ["number"], name: "index_products_on_number", using: :btree

  create_table "system_users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "system_users", ["email"], name: "index_system_users_on_email", unique: true, using: :btree
  add_index "system_users", ["reset_password_token"], name: "index_system_users_on_reset_password_token", unique: true, using: :btree

end
