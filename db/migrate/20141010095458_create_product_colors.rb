class CreateProductColors < ActiveRecord::Migration
  def change
    create_table :product_colors do |t|
      t.references :product, index: true
      t.references :base_color, index: true

      t.timestamps
    end
  end
end
