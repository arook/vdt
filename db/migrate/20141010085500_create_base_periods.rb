class CreateBasePeriods < ActiveRecord::Migration
  def change
    create_table :base_periods do |t|
      t.string :code
      t.string :name

      t.timestamps
    end
    add_index :base_periods, :code
  end
end
