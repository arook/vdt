class CreateProductSizes < ActiveRecord::Migration
  def change
    create_table :product_sizes do |t|
      t.references :product, index: true
      t.references :base_size, index: true

      t.timestamps
    end
  end
end
