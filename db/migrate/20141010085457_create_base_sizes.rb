class CreateBaseSizes < ActiveRecord::Migration
  def change
    create_table :base_sizes do |t|
      t.string :class
      t.integer :index
      t.string :name
      t.string :name
      t.string :value

      t.timestamps
    end
    add_index :base_sizes, :class
    add_index :base_sizes, :name
  end
end
