class CreateBaseClassifieds < ActiveRecord::Migration
  def change
    create_table :base_classifieds do |t|
      t.string :code
      t.string :name

      t.timestamps
    end
    add_index :base_classifieds, :code
  end
end
