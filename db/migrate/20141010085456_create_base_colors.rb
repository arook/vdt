class CreateBaseColors < ActiveRecord::Migration
  def change
    create_table :base_colors do |t|
      t.string :code
      t.string :name

      t.timestamps
    end
    add_index :base_colors, :code
  end
end
