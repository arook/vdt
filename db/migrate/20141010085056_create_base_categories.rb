class CreateBaseCategories < ActiveRecord::Migration
  def change
    create_table :base_categories do |t|
      t.integer :parent_id
      t.string :code
      t.string :name

      t.timestamps
    end
    add_index :base_categories, :code
  end
end
