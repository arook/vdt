class CreateBaseCustomers < ActiveRecord::Migration
  def change
    create_table :base_customers do |t|
      t.string :original
      t.string :area
      t.string :parent
      t.string :level
      t.string :storename
      t.string :login
      t.string :code
      t.string :password
      t.integer :amount
      t.integer :quantity
      t.boolean :if_enable

      t.timestamps
    end
    add_index :base_customers, :original
    add_index :base_customers, :area
    add_index :base_customers, :storename
    add_index :base_customers, :login, unique: true
  end
end
