class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :number
      t.string :name
      t.string :size_class
      t.decimal :price, precision: 7, scale: 2
      t.integer :series_id
      t.integer :period_id
      t.integer :classified_id
      t.integer :category_id
      t.integer :sub_category_id
      t.string :recommended_level
      t.text :description_fabric
      t.text :description_style
      t.boolean :if_ordering
      t.boolean :if_replenishing
      t.boolean :if_onsale
      t.boolean :if_preselection
      t.string :sex
      t.string :catena
      t.date :topdate

      t.timestamps
    end
    add_index :products, :number
    add_index :products, :if_ordering
    add_index :products, :if_replenishing
    add_index :products, :if_onsale
    add_index :products, :if_preselection
  end
end
