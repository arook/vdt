class CreateBaseSeries < ActiveRecord::Migration
  def change
    create_table :base_series do |t|
      t.string :code
      t.string :name

      t.timestamps
    end
    add_index :base_series, :code
  end
end
