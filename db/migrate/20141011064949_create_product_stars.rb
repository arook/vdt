class CreateProductStars < ActiveRecord::Migration
  def change
    create_table :product_stars do |t|
      t.references :product, index: true
      t.references :product_color, index: true
      t.references :base_customer, index: true
      t.integer :star

      t.timestamps
    end
  end
end
