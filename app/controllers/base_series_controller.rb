class BaseSeriesController < ApplicationController
  before_action :set_base_series, only: [:show, :edit, :update, :destroy]

  # GET /base_series
  # GET /base_series.json
  def index
    @base_series = BaseSeries.all
  end

  # GET /base_series/1
  # GET /base_series/1.json
  def show
  end

  # GET /base_series/new
  def new
    @base_series = BaseSeries.new
  end

  # GET /base_series/1/edit
  def edit
  end

  # POST /base_series
  # POST /base_series.json
  def create
    @base_series = BaseSeries.new(base_series_params)

    respond_to do |format|
      if @base_series.save
        format.html { redirect_to @base_series, notice: 'Base series was successfully created.' }
        format.json { render :show, status: :created, location: @base_series }
      else
        format.html { render :new }
        format.json { render json: @base_series.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /base_series/1
  # PATCH/PUT /base_series/1.json
  def update
    respond_to do |format|
      if @base_series.update(base_series_params)
        format.html { redirect_to @base_series, notice: 'Base series was successfully updated.' }
        format.json { render :show, status: :ok, location: @base_series }
      else
        format.html { render :edit }
        format.json { render json: @base_series.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /base_series/1
  # DELETE /base_series/1.json
  def destroy
    @base_series.destroy
    respond_to do |format|
      format.html { redirect_to base_series_index_url, notice: 'Base series was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_base_series
      @base_series = BaseSeries.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def base_series_params
      params.require(:base_series).permit(:code, :name)
    end
end
