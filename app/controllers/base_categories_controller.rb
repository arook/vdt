class BaseCategoriesController < ApplicationController
  before_action :set_base_category, only: [:show, :edit, :update, :destroy]

  # GET /base_categories
  # GET /base_categories.json
  def index
    @base_categories = BaseCategory.all
  end

  # GET /base_categories/1
  # GET /base_categories/1.json
  def show
  end

  # GET /base_categories/new
  def new
    @base_category = BaseCategory.new
  end

  # GET /base_categories/1/edit
  def edit
  end

  # POST /base_categories
  # POST /base_categories.json
  def create
    @base_category = BaseCategory.new(base_category_params)

    respond_to do |format|
      if @base_category.save
        format.html { redirect_to @base_category, notice: 'Base category was successfully created.' }
        format.json { render :show, status: :created, location: @base_category }
      else
        format.html { render :new }
        format.json { render json: @base_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /base_categories/1
  # PATCH/PUT /base_categories/1.json
  def update
    respond_to do |format|
      if @base_category.update(base_category_params)
        format.html { redirect_to @base_category, notice: 'Base category was successfully updated.' }
        format.json { render :show, status: :ok, location: @base_category }
      else
        format.html { render :edit }
        format.json { render json: @base_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /base_categories/1
  # DELETE /base_categories/1.json
  def destroy
    @base_category.destroy
    respond_to do |format|
      format.html { redirect_to base_categories_url, notice: 'Base category was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_base_category
      @base_category = BaseCategory.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def base_category_params
      params.require(:base_category).permit(:parent_id, :code, :name)
    end
end
