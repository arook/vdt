class BaseSizesController < ApplicationController
  before_action :set_base_size, only: [:show, :edit, :update, :destroy]

  # GET /base_sizes
  # GET /base_sizes.json
  def index
    @base_sizes = BaseSize.all
  end

  # GET /base_sizes/1
  # GET /base_sizes/1.json
  def show
  end

  # GET /base_sizes/new
  def new
    @base_size = BaseSize.new
  end

  # GET /base_sizes/1/edit
  def edit
  end

  # POST /base_sizes
  # POST /base_sizes.json
  def create
    @base_size = BaseSize.new(base_size_params)

    respond_to do |format|
      if @base_size.save
        format.html { redirect_to @base_size, notice: 'Base size was successfully created.' }
        format.json { render :show, status: :created, location: @base_size }
      else
        format.html { render :new }
        format.json { render json: @base_size.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /base_sizes/1
  # PATCH/PUT /base_sizes/1.json
  def update
    respond_to do |format|
      if @base_size.update(base_size_params)
        format.html { redirect_to @base_size, notice: 'Base size was successfully updated.' }
        format.json { render :show, status: :ok, location: @base_size }
      else
        format.html { render :edit }
        format.json { render json: @base_size.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /base_sizes/1
  # DELETE /base_sizes/1.json
  def destroy
    @base_size.destroy
    respond_to do |format|
      format.html { redirect_to base_sizes_url, notice: 'Base size was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_base_size
      @base_size = BaseSize.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def base_size_params
      params.require(:base_size).permit(:class, :index, :name, :name, :value)
    end
end
