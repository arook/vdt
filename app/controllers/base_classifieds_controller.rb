class BaseClassifiedsController < ApplicationController
  before_action :set_base_classified, only: [:show, :edit, :update, :destroy]

  # GET /base_classifieds
  # GET /base_classifieds.json
  def index
    @base_classifieds = BaseClassified.all
  end

  # GET /base_classifieds/1
  # GET /base_classifieds/1.json
  def show
  end

  # GET /base_classifieds/new
  def new
    @base_classified = BaseClassified.new
  end

  # GET /base_classifieds/1/edit
  def edit
  end

  # POST /base_classifieds
  # POST /base_classifieds.json
  def create
    @base_classified = BaseClassified.new(base_classified_params)

    respond_to do |format|
      if @base_classified.save
        format.html { redirect_to @base_classified, notice: 'Base classified was successfully created.' }
        format.json { render :show, status: :created, location: @base_classified }
      else
        format.html { render :new }
        format.json { render json: @base_classified.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /base_classifieds/1
  # PATCH/PUT /base_classifieds/1.json
  def update
    respond_to do |format|
      if @base_classified.update(base_classified_params)
        format.html { redirect_to @base_classified, notice: 'Base classified was successfully updated.' }
        format.json { render :show, status: :ok, location: @base_classified }
      else
        format.html { render :edit }
        format.json { render json: @base_classified.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /base_classifieds/1
  # DELETE /base_classifieds/1.json
  def destroy
    @base_classified.destroy
    respond_to do |format|
      format.html { redirect_to base_classifieds_url, notice: 'Base classified was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_base_classified
      @base_classified = BaseClassified.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def base_classified_params
      params.require(:base_classified).permit(:code, :name)
    end
end
