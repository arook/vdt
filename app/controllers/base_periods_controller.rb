class BasePeriodsController < ApplicationController
  before_action :set_base_period, only: [:show, :edit, :update, :destroy]

  # GET /base_periods
  # GET /base_periods.json
  def index
    @base_periods = BasePeriod.all
  end

  # GET /base_periods/1
  # GET /base_periods/1.json
  def show
  end

  # GET /base_periods/new
  def new
    @base_period = BasePeriod.new
  end

  # GET /base_periods/1/edit
  def edit
  end

  # POST /base_periods
  # POST /base_periods.json
  def create
    @base_period = BasePeriod.new(base_period_params)

    respond_to do |format|
      if @base_period.save
        format.html { redirect_to @base_period, notice: 'Base period was successfully created.' }
        format.json { render :show, status: :created, location: @base_period }
      else
        format.html { render :new }
        format.json { render json: @base_period.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /base_periods/1
  # PATCH/PUT /base_periods/1.json
  def update
    respond_to do |format|
      if @base_period.update(base_period_params)
        format.html { redirect_to @base_period, notice: 'Base period was successfully updated.' }
        format.json { render :show, status: :ok, location: @base_period }
      else
        format.html { render :edit }
        format.json { render json: @base_period.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /base_periods/1
  # DELETE /base_periods/1.json
  def destroy
    @base_period.destroy
    respond_to do |format|
      format.html { redirect_to base_periods_url, notice: 'Base period was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_base_period
      @base_period = BasePeriod.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def base_period_params
      params.require(:base_period).permit(:code, :name)
    end
end
