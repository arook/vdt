class BaseCustomersController < ApplicationController
  before_action :set_base_customer, only: [:show, :edit, :update, :destroy]

  # GET /base_customers
  # GET /base_customers.json
  def index
    @base_customers = BaseCustomer.all
  end

  # GET /base_customers/1
  # GET /base_customers/1.json
  def show
  end

  # GET /base_customers/new
  def new
    @base_customer = BaseCustomer.new
  end

  # GET /base_customers/1/edit
  def edit
  end

  # POST /base_customers
  # POST /base_customers.json
  def create
    @base_customer = BaseCustomer.new(base_customer_params)

    respond_to do |format|
      if @base_customer.save
        format.html { redirect_to @base_customer, notice: 'Base customer was successfully created.' }
        format.json { render :show, status: :created, location: @base_customer }
      else
        format.html { render :new }
        format.json { render json: @base_customer.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /base_customers/1
  # PATCH/PUT /base_customers/1.json
  def update
    respond_to do |format|
      if @base_customer.update(base_customer_params)
        format.html { redirect_to @base_customer, notice: 'Base customer was successfully updated.' }
        format.json { render :show, status: :ok, location: @base_customer }
      else
        format.html { render :edit }
        format.json { render json: @base_customer.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /base_customers/1
  # DELETE /base_customers/1.json
  def destroy
    @base_customer.destroy
    respond_to do |format|
      format.html { redirect_to base_customers_url, notice: 'Base customer was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_base_customer
      @base_customer = BaseCustomer.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def base_customer_params
      params.require(:base_customer).permit(:original, :area, :parent, :level, :storename, :login, :code, :password, :amount, :quantity, :if_enable)
    end
end
