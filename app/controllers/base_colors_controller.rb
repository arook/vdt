class BaseColorsController < ApplicationController
  before_action :set_base_color, only: [:show, :edit, :update, :destroy]

  # GET /base_colors
  # GET /base_colors.json
  def index
    @base_colors = BaseColor.all
  end

  # GET /base_colors/1
  # GET /base_colors/1.json
  def show
  end

  # GET /base_colors/new
  def new
    @base_color = BaseColor.new
  end

  # GET /base_colors/1/edit
  def edit
  end

  # POST /base_colors
  # POST /base_colors.json
  def create
    @base_color = BaseColor.new(base_color_params)

    respond_to do |format|
      if @base_color.save
        format.html { redirect_to @base_color, notice: 'Base color was successfully created.' }
        format.json { render :show, status: :created, location: @base_color }
      else
        format.html { render :new }
        format.json { render json: @base_color.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /base_colors/1
  # PATCH/PUT /base_colors/1.json
  def update
    respond_to do |format|
      if @base_color.update(base_color_params)
        format.html { redirect_to @base_color, notice: 'Base color was successfully updated.' }
        format.json { render :show, status: :ok, location: @base_color }
      else
        format.html { render :edit }
        format.json { render json: @base_color.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /base_colors/1
  # DELETE /base_colors/1.json
  def destroy
    @base_color.destroy
    respond_to do |format|
      format.html { redirect_to base_colors_url, notice: 'Base color was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_base_color
      @base_color = BaseColor.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def base_color_params
      params.require(:base_color).permit(:code, :name)
    end
end
