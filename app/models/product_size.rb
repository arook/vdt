class ProductSize < ActiveRecord::Base
  belongs_to :product
  belongs_to :base_size
end
