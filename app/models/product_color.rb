class ProductColor < ActiveRecord::Base
  belongs_to :product
  belongs_to :base_color
end
