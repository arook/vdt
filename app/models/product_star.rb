class ProductStar < ActiveRecord::Base
  belongs_to :product
  belongs_to :product_color
  belongs_to :base_customer
end
