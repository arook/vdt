json.array!(@base_periods) do |base_period|
  json.extract! base_period, :id, :code, :name
  json.url base_period_url(base_period, format: :json)
end
