json.array!(@base_categories) do |base_category|
  json.extract! base_category, :id, :parent_id, :code, :name
  json.url base_category_url(base_category, format: :json)
end
