json.array!(@base_colors) do |base_color|
  json.extract! base_color, :id, :code, :name
  json.url base_color_url(base_color, format: :json)
end
