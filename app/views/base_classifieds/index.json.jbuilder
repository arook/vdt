json.array!(@base_classifieds) do |base_classified|
  json.extract! base_classified, :id, :code, :name
  json.url base_classified_url(base_classified, format: :json)
end
