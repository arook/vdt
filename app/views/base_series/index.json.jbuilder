json.array!(@base_series) do |base_series|
  json.extract! base_series, :id, :code, :name
  json.url base_series_url(base_series, format: :json)
end
