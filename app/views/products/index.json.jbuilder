json.array!(@products) do |product|
  json.extract! product, :id, :number, :name, :size_class, :price, :series_id, :period_id, :classified_id, :category_id, :sub_category_id, :recommended_level, :description_fabric, :description_style, :if_ordering, :if_replenishing, :if_onsale, :if_preselection, :sex, :catena, :topdate
  json.url product_url(product, format: :json)
end
