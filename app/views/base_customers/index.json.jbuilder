json.array!(@base_customers) do |base_customer|
  json.extract! base_customer, :id, :original, :area, :parent, :level, :storename, :login, :code, :password, :amount, :quantity, :if_enable
  json.url base_customer_url(base_customer, format: :json)
end
