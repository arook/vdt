require 'test_helper'

class BaseCustomersControllerTest < ActionController::TestCase
  setup do
    @base_customer = base_customers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:base_customers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create base_customer" do
    assert_difference('BaseCustomer.count') do
      post :create, base_customer: { amount: @base_customer.amount, area: @base_customer.area, code: @base_customer.code, if_enable: @base_customer.if_enable, level: @base_customer.level, login: @base_customer.login, original: @base_customer.original, parent: @base_customer.parent, password: @base_customer.password, quantity: @base_customer.quantity, storename: @base_customer.storename }
    end

    assert_redirected_to base_customer_path(assigns(:base_customer))
  end

  test "should show base_customer" do
    get :show, id: @base_customer
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @base_customer
    assert_response :success
  end

  test "should update base_customer" do
    patch :update, id: @base_customer, base_customer: { amount: @base_customer.amount, area: @base_customer.area, code: @base_customer.code, if_enable: @base_customer.if_enable, level: @base_customer.level, login: @base_customer.login, original: @base_customer.original, parent: @base_customer.parent, password: @base_customer.password, quantity: @base_customer.quantity, storename: @base_customer.storename }
    assert_redirected_to base_customer_path(assigns(:base_customer))
  end

  test "should destroy base_customer" do
    assert_difference('BaseCustomer.count', -1) do
      delete :destroy, id: @base_customer
    end

    assert_redirected_to base_customers_path
  end
end
