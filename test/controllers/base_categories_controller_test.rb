require 'test_helper'

class BaseCategoriesControllerTest < ActionController::TestCase
  setup do
    @base_category = base_categories(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:base_categories)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create base_category" do
    assert_difference('BaseCategory.count') do
      post :create, base_category: { code: @base_category.code, name: @base_category.name, parent_id: @base_category.parent_id }
    end

    assert_redirected_to base_category_path(assigns(:base_category))
  end

  test "should show base_category" do
    get :show, id: @base_category
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @base_category
    assert_response :success
  end

  test "should update base_category" do
    patch :update, id: @base_category, base_category: { code: @base_category.code, name: @base_category.name, parent_id: @base_category.parent_id }
    assert_redirected_to base_category_path(assigns(:base_category))
  end

  test "should destroy base_category" do
    assert_difference('BaseCategory.count', -1) do
      delete :destroy, id: @base_category
    end

    assert_redirected_to base_categories_path
  end
end
