require 'test_helper'

class ProductsControllerTest < ActionController::TestCase
  setup do
    @product = products(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:products)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create product" do
    assert_difference('Product.count') do
      post :create, product: { category_id: @product.category_id, catena: @product.catena, classified_id: @product.classified_id, description_fabric: @product.description_fabric, description_style: @product.description_style, if_onsale: @product.if_onsale, if_ordering: @product.if_ordering, if_preselection: @product.if_preselection, if_replenishing: @product.if_replenishing, name: @product.name, number: @product.number, period_id: @product.period_id, price: @product.price, recommended_level: @product.recommended_level, series_id: @product.series_id, sex: @product.sex, size_class: @product.size_class, sub_category_id: @product.sub_category_id, topdate: @product.topdate }
    end

    assert_redirected_to product_path(assigns(:product))
  end

  test "should show product" do
    get :show, id: @product
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @product
    assert_response :success
  end

  test "should update product" do
    patch :update, id: @product, product: { category_id: @product.category_id, catena: @product.catena, classified_id: @product.classified_id, description_fabric: @product.description_fabric, description_style: @product.description_style, if_onsale: @product.if_onsale, if_ordering: @product.if_ordering, if_preselection: @product.if_preselection, if_replenishing: @product.if_replenishing, name: @product.name, number: @product.number, period_id: @product.period_id, price: @product.price, recommended_level: @product.recommended_level, series_id: @product.series_id, sex: @product.sex, size_class: @product.size_class, sub_category_id: @product.sub_category_id, topdate: @product.topdate }
    assert_redirected_to product_path(assigns(:product))
  end

  test "should destroy product" do
    assert_difference('Product.count', -1) do
      delete :destroy, id: @product
    end

    assert_redirected_to products_path
  end
end
