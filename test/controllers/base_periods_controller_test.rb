require 'test_helper'

class BasePeriodsControllerTest < ActionController::TestCase
  setup do
    @base_period = base_periods(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:base_periods)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create base_period" do
    assert_difference('BasePeriod.count') do
      post :create, base_period: { code: @base_period.code, name: @base_period.name }
    end

    assert_redirected_to base_period_path(assigns(:base_period))
  end

  test "should show base_period" do
    get :show, id: @base_period
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @base_period
    assert_response :success
  end

  test "should update base_period" do
    patch :update, id: @base_period, base_period: { code: @base_period.code, name: @base_period.name }
    assert_redirected_to base_period_path(assigns(:base_period))
  end

  test "should destroy base_period" do
    assert_difference('BasePeriod.count', -1) do
      delete :destroy, id: @base_period
    end

    assert_redirected_to base_periods_path
  end
end
