require 'test_helper'

class BaseSeriesControllerTest < ActionController::TestCase
  setup do
    @base_series = base_series(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:base_series)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create base_series" do
    assert_difference('BaseSeries.count') do
      post :create, base_series: { code: @base_series.code, name: @base_series.name }
    end

    assert_redirected_to base_series_path(assigns(:base_series))
  end

  test "should show base_series" do
    get :show, id: @base_series
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @base_series
    assert_response :success
  end

  test "should update base_series" do
    patch :update, id: @base_series, base_series: { code: @base_series.code, name: @base_series.name }
    assert_redirected_to base_series_path(assigns(:base_series))
  end

  test "should destroy base_series" do
    assert_difference('BaseSeries.count', -1) do
      delete :destroy, id: @base_series
    end

    assert_redirected_to base_series_index_path
  end
end
