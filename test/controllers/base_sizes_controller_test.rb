require 'test_helper'

class BaseSizesControllerTest < ActionController::TestCase
  setup do
    @base_size = base_sizes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:base_sizes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create base_size" do
    assert_difference('BaseSize.count') do
      post :create, base_size: { class: @base_size.class, index: @base_size.index, name: @base_size.name, name: @base_size.name, value: @base_size.value }
    end

    assert_redirected_to base_size_path(assigns(:base_size))
  end

  test "should show base_size" do
    get :show, id: @base_size
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @base_size
    assert_response :success
  end

  test "should update base_size" do
    patch :update, id: @base_size, base_size: { class: @base_size.class, index: @base_size.index, name: @base_size.name, name: @base_size.name, value: @base_size.value }
    assert_redirected_to base_size_path(assigns(:base_size))
  end

  test "should destroy base_size" do
    assert_difference('BaseSize.count', -1) do
      delete :destroy, id: @base_size
    end

    assert_redirected_to base_sizes_path
  end
end
