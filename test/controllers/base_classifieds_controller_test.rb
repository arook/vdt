require 'test_helper'

class BaseClassifiedsControllerTest < ActionController::TestCase
  setup do
    @base_classified = base_classifieds(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:base_classifieds)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create base_classified" do
    assert_difference('BaseClassified.count') do
      post :create, base_classified: { code: @base_classified.code, name: @base_classified.name }
    end

    assert_redirected_to base_classified_path(assigns(:base_classified))
  end

  test "should show base_classified" do
    get :show, id: @base_classified
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @base_classified
    assert_response :success
  end

  test "should update base_classified" do
    patch :update, id: @base_classified, base_classified: { code: @base_classified.code, name: @base_classified.name }
    assert_redirected_to base_classified_path(assigns(:base_classified))
  end

  test "should destroy base_classified" do
    assert_difference('BaseClassified.count', -1) do
      delete :destroy, id: @base_classified
    end

    assert_redirected_to base_classifieds_path
  end
end
